#include "Block.h"

Block::Block(int size, int color[3]) {
    if (size < 2) size = 2;
    else if (size > 3) size = 3;
    
    this->size = size;

    for (int i = 0; i < 3; ++i) {
        if (color[i] > 255) color[i] = 255;
        else if (color[i] < 0) color[i] = 0;
        this->color[i] = color[i];
    }
}

// Implementazione del metodo getSize
int Block::getSize() {
    return size;
}

// Implementazione del metodo getColor
int* Block::getColor() {
    return color;
}