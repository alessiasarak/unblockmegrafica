#include "Board.h"

Board::Board(std::vector<Block> blocks) {
    // Inizializza il vettore di oggetti blocks
    this->blocks = blocks;

    // Inizializza l'oggetto specialBlock
    int color[] = {255,0,0};
    this->specialBlock = Block(2, color);

}