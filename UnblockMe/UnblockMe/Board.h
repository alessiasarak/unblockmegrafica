#pragma once
#include <vector>
#include "Block.h"

class Board
{
public:
	static const int size = 6;
	// Costruttore della classe Board
	Board(std::vector<Block> blocks);
private:
	int board[size][size];
	std::vector<Block> blocks;
	Block specialBlock;
};

