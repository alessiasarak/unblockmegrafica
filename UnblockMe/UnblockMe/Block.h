#pragma once
#include <string>

class Block
{
private:
    int id;
    int size;
    int color[3]; //[r;g;b]

public:
    Block(int size, int color[3]);

    int getSize();
    int* getColor();
};

